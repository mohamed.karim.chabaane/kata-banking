package com.publicissapient.web.controllers;

import com.publicissapient.dtos.OperationDto;
import com.publicissapient.enums.OperationType;
import com.publicissapient.services.impl.OperationService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OperationController.class)
class OperationControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OperationService operationService;


    @Test
    public void allOperations() throws Exception {

        when(operationService.allOperations()).thenReturn(new ArrayList<>());
        mvc.perform(MockMvcRequestBuilders
                .get("/Bank-Kata/operations")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deposit() throws Exception {

        OperationDto deposit = new OperationDto();
        deposit.setAmount(100D);
        deposit.setId(1);
        deposit.setDate(LocalDateTime.now());
        deposit.setType(OperationType.DEPOSIT);
        deposit.setBankAccountBalance(1000D);
        deposit.setBankAccountClientLastName("CHABAANE");

        when(operationService.deposit(anyDouble())).thenReturn(deposit);
        mvc.perform(MockMvcRequestBuilders
                .post("/Bank-Kata/deposit")
                .content("{\"amount\":100}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.bankAccountBalance").value(1000));
    }

    @Test
    public void withdraw() throws Exception {

        OperationDto withdraw = new OperationDto();
        withdraw.setAmount(100D);
        withdraw.setId(1);
        withdraw.setDate(LocalDateTime.now());
        withdraw.setType(OperationType.DEPOSIT);
        withdraw.setBankAccountBalance(0D);
        withdraw.setBankAccountClientLastName("CHABAANE");

        when(operationService.withdraw(anyDouble())).thenReturn(withdraw);
        mvc.perform(MockMvcRequestBuilders
                .post("/Bank-Kata/withdraw")
                .content("{\"amount\":100}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.bankAccountBalance").value(0));
    }

}
