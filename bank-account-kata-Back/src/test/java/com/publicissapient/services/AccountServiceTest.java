package com.publicissapient.services;

import com.publicissapient.dtos.AccountDto;
import com.publicissapient.dtos.AccountRequest;
import com.publicissapient.dtos.OperationDto;
import com.publicissapient.entities.BankAccount;
import com.publicissapient.entities.Client;
import com.publicissapient.entities.Operation;
import com.publicissapient.repositories.BankAccountRepository;
import com.publicissapient.repositories.OperationRepository;
import com.publicissapient.services.impl.AccountService;
import com.publicissapient.services.impl.OperationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class AccountServiceTest {

    @Autowired
    AccountService accountService;

    @MockBean
    BankAccountRepository bankAccountRepository;


    @BeforeEach
    void setUp() {

        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(1000D);
        bankAccount.setClient(new Client());
        bankAccount.getClient().setFirstName("Karim");
        bankAccount.getClient().setLastName("CHABAANE");
        when(bankAccountRepository.findByAccountIdAndAccountCode(anyString(), anyInt())).thenReturn(bankAccount);

    }

    @Test
    void login() {
        AccountRequest accountRequest = new AccountRequest("12345", 1234);
        AccountDto accountDto = accountService.login(accountRequest);
        assertNotNull(accountDto);
        assertEquals("Karim", accountDto.getClientFirstName());
        assertEquals("CHABAANE", accountDto.getClientLastName());
    }
}
