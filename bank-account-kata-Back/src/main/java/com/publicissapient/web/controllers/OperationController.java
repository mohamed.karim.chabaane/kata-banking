package com.publicissapient.web.controllers;

import com.publicissapient.dtos.OperationDto;
import com.publicissapient.dtos.OperationRequestBody;
import com.publicissapient.services.IOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/Bank-Kata")
@RestController
public class OperationController {

    @Autowired
    private IOperationService operationService;

    @GetMapping(value="/operations")
    public List<OperationDto> getAllOperations() {
        return operationService.allOperations();
    }

    @GetMapping(value="/balance")
    public Double getBalance() {
        return operationService.getBalance();
    }

    @PostMapping(value = "/deposit")
    public ResponseEntity<OperationDto> deposit(@RequestBody OperationRequestBody operation) {
        OperationDto deposit = operationService.deposit(operation.getAmount());

        if (deposit == null)
            return ResponseEntity.noContent().build();

        return ResponseEntity.status(HttpStatus.OK).body(deposit);
    }

    @PostMapping(value = "/withdraw")
    public ResponseEntity<OperationDto> withdraw(@RequestBody OperationRequestBody operation) {
        OperationDto withdraw = operationService.withdraw(operation.getAmount());

        if (withdraw == null)
            return ResponseEntity.noContent().build();

        return ResponseEntity.status(HttpStatus.OK).body(withdraw);
    }

}
