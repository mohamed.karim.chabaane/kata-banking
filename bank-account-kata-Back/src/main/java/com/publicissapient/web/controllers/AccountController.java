package com.publicissapient.web.controllers;

import com.publicissapient.dtos.AccountDto;
import com.publicissapient.dtos.AccountRequest;
import com.publicissapient.services.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/client")
public class AccountController {

    @Autowired
    private IAccountService accountService;

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE
                                   , produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Object> login(@RequestBody @NotNull AccountRequest accountRequest) {

            AccountDto accountDto = accountService.login(accountRequest);
            if (accountDto == null) {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok(accountDto);
    }
}
