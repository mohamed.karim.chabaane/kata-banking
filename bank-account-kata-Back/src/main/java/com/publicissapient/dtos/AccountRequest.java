package com.publicissapient.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountRequest {

    @JsonProperty("account_id")
    private String account_id;

    @JsonProperty("account_code")
    private int account_code;

    public AccountRequest() {};

    public AccountRequest(String account_id, int account_code) {
        this.account_id = account_id;
        this.account_code = account_code;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public int getAccount_code() {
        return account_code;
    }

    public void setAccount_code(int account_code) {
        this.account_code = account_code;
    }
}
