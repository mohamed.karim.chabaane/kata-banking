package com.publicissapient.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDate;

public class AccountDto implements Serializable {


    private int id;

    @JsonProperty("account_id")
    private String account_id;

    @JsonProperty("account_code")
    private int account_code;

    @JsonProperty("balance")
    private Double balance;

    @JsonProperty("creationDate")
    private LocalDate creationDate;

    @JsonProperty("clientFirstName")
    private String clientFirstName;

    @JsonProperty("clientLastName")
    private String clientLastName;

    @JsonProperty("clientAddress")
    private String clientAddress;

    @JsonProperty("clientPhone")
    private String clientPhone;

    public AccountDto() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public int getAccount_code() {
        return account_code;
    }

    public void setAccount_code(int account_code) {
        this.account_code = account_code;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }
}
