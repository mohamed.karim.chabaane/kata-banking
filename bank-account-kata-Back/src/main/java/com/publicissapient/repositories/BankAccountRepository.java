package com.publicissapient.repositories;

import com.publicissapient.entities.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
    BankAccount findById(int id);
    @Query("SELECT  b FROM BankAccount b WHERE b.account_id = ?1 and b.account_code = ?2")
    BankAccount findByAccountIdAndAccountCode(String account_id, int account_code);
}
