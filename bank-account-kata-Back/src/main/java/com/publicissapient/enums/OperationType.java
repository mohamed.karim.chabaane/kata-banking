package com.publicissapient.enums;

public enum OperationType {
	DEPOSIT,
	WITHDRAWAL
}
