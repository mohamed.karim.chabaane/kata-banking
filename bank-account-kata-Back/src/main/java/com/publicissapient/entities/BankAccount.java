package com.publicissapient.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Entity
public class BankAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull(message = "Account_id is mandatory")
	@Column(name = "account_id")
	private String account_id;

	@NotNull(message = "Account_code is mandatory")
	@Column(name = "account_code")
	private int account_code;

	private Double balance;

	@Column(name = "creationDate")
	private LocalDate creationDate;

	@NotNull(message = "Client is mandatory")
	@OneToOne
	private Client client;

	public BankAccount() {
		this.creationDate = LocalDate.now();
		this.balance = 0d;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount_id() { return account_id; }

	public void setAccount_id(String account_id) { this.account_id = account_id; }

	public int getAccount_code() { return account_code; }

	public void setAccount_code(int account_code) { this.account_code = account_code; }
}
