package com.publicissapient.services.impl;

import com.publicissapient.dtos.AccountDto;
import com.publicissapient.dtos.AccountRequest;
import com.publicissapient.entities.BankAccount;
import com.publicissapient.repositories.BankAccountRepository;
import com.publicissapient.services.IAccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService implements IAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public AccountDto login(AccountRequest accountRequest) {

        BankAccount bankAccount = this.bankAccountRepository.findByAccountIdAndAccountCode(accountRequest.getAccount_id(), accountRequest.getAccount_code());
        if (bankAccount == null) {
            return null;
        } else {

                return modelMapper.map(bankAccount, AccountDto.class);
        }
    }
}
