package com.publicissapient.services;

import com.publicissapient.dtos.OperationDto;

import java.util.List;


public interface IOperationService {

    OperationDto deposit(Double amount);

    OperationDto withdraw(Double amount);

    List<OperationDto> allOperations();

    Double getBalance();
}
