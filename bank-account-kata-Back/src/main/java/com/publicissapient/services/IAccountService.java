package com.publicissapient.services;

import com.publicissapient.dtos.AccountDto;
import com.publicissapient.dtos.AccountRequest;
import com.publicissapient.exception.BankException;

public interface IAccountService {

    AccountDto login(AccountRequest accountRequest);
}
