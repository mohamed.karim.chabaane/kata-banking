import { Component, OnInit } from '@angular/core';
import {Client} from "../../models/client";
import {IOperation} from "../../models/Ioperation";
import {OperationService} from "../../services/operation.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  operations : IOperation[] | undefined;

  constructor(private operationService : OperationService,
              private _router: Router) { }

  ngOnInit() {

    //on recherche toutes les operations
      this.operationService.listOperation()
        .subscribe( listOperations => {this.operations = listOperations;},
          e => console.log(e.message));

  }

}
