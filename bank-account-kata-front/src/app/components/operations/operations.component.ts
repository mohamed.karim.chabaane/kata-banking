import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {OperationService} from "../../services/operation.service";
import {ClientService} from "../../services/client.service";
import {OperationRequest} from "../../models/operationRequest";

@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.scss']
})
export class OperationsComponent implements OnInit {

  private operation: OperationRequest;
  errors: string = "";
  validation: string = "";
  amount: number = 0;
  balance: number | undefined;


  constructor(private operationService : OperationService,
              private clientService: ClientService,
              private _router: Router) {

    this.operation = new OperationRequest(0);
  }


  ngOnInit() {

    this.errors = "";
    this.validation = "";

  }//fin ngOnINit




  toDoDeposit(){

    this.operation = new OperationRequest(this.amount)
    this.operationService.deposit(this.operation)
      .subscribe( data => {
          this.balance = data.bankAccountBalance;
        console.log(this.balance)
          this.validation = "deposit OK" ;
          this.errors ="";
        },
        e => {
          this.validation = "" ;
          this.errors = "Error during validation" ;
        }
      );
  }



  toDoWithdrawal(){

    this.operation = new OperationRequest(this.amount);
    this.operation.amount = this.amount;
    this.operationService.withdrawal(this.operation)
      .subscribe( data  => {
          this.balance = data.bankAccountBalance
          this.validation = "withdrawal OK" ;
          this.errors ="";
        },
        e => {
          this.validation = "" ;
          this.errors = "Error, check if you have enough money in your account please !" ;
        }
      );

  }

}
