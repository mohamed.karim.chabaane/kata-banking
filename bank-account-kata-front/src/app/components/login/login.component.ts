import { Component, OnInit } from '@angular/core';
import {ClientService} from "../../services/client.service";
import {AccountRequest} from "../../models/accountRequest";
import {Client} from "../../models/client";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  account_id: string;
  account_code: number;
  client: Client | undefined;
  errors: string = "";

  constructor(private clientService: ClientService,
              private router: Router) {
    this.account_id =  "1234578";
    this.account_code = 520902;
  }

  ngOnInit(): void {

  }

  login() {
     const accountRequest: AccountRequest = new AccountRequest(this.account_id, this.account_code);
     this.clientService.login(accountRequest).subscribe(data => {
        this.client = new Client(data.clientFirstName, data.clientLastName, data.clientAddress, data.clientPhone, data.balance);
        sessionStorage.setItem("client", JSON.stringify(this.client));
        this.router.navigate(["client"]);
       },
       error => {
          this.errors = "ID or code Account are wrong";
       })

  }
}
