import {Component, Input, OnInit} from '@angular/core';
import {Client} from "../../models/client";
import {OperationService} from "../../services/operation.service";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  client: Client = new Client("Karim","CHABAANE","66 Avenue Raymond Aron 91300 Massy", "0638359512", 1000);

  @Input()
  balance: number | undefined;

  constructor(private operationService :OperationService) { }

  ngOnInit(): void {
    const clientString: string | null = sessionStorage.getItem("client");
    if (clientString) {
      this.client = JSON.parse(clientString);
    }
    if (!this.balance) {
      this.operationService.getBalance().subscribe(balance => this.balance = balance);
    }
  }

}
