export interface IOperation {
   OperationType: string;
   amount: number;
   date: string;
   bankAccountBalance: number;
}
