
export class Client {

  clientFirstName: string;
  clientLastName: string;
  clientAddress: string;
  clientPhone: string;
  balance: number;

    constructor(clientFirstName: string,
                clientLastName: string,
                clientAddress: string,
                clientPhone: string,
                balance: number) {
       this.clientFirstName = clientFirstName;
       this.clientLastName = clientLastName;
       this.clientAddress =  clientAddress;
       this.clientPhone =  clientPhone;
       this.balance = balance;
     }

}
