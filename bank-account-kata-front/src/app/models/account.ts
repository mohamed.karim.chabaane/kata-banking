export interface IAccount {

  account_id: string;
  account_code: number;
  balance: number;
  creationDate: string;
  clientFirstName: string;
  clientLastName: string;
  clientAddress: string;
  clientPhone: string;
}
