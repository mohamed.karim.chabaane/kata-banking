export class OperationRequest {

   amount: number;

  constructor(amount: number) {
    this.amount = amount;
  }
}
