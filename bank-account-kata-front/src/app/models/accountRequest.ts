export class AccountRequest {

  private account_id: string;
  private account_code: number;

  public constructor(account_id: string,
                     account_code: number) {

    this.account_id = account_id;
    this.account_code = account_code;

  }
}
