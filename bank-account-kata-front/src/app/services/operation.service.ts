import { Injectable } from '@angular/core';
import {AppUrlConfig} from "../app.url-config";
import {HttpClient} from "@angular/common/http";
import {IOperation} from "../models/Ioperation";
import {Observable} from "rxjs";
import {OperationRequest} from "../models/operationRequest";

@Injectable({
  providedIn: 'root'
})
export class OperationService {

  private urlWebService_begin: string = AppUrlConfig.URL_WEBSERVICE_BEGIN;

  constructor(private _http: HttpClient) {
    // _http injecté ici servira a appeler des WS REST
  }


  //Retrait
  public withdrawal(operation: OperationRequest): Observable<IOperation> {

    let urlWS: string = this.urlWebService_begin + AppUrlConfig.URL_API_WITHDRAW
    const headers = { 'content-type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'}
    return this._http.post<IOperation>(urlWS, JSON.stringify(operation), {'headers':headers});

  }


  //Faire un depot
  public deposit(operation: OperationRequest): Observable<IOperation> {

    let urlWS: string = this.urlWebService_begin + AppUrlConfig.URL_API_DEPOSIT;
    const headers = { 'content-type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'}
    return this._http.post<IOperation>(urlWS, JSON.stringify(operation), {'headers':headers});

  }


  //list all  operations
  public listOperation(): Observable<IOperation[]> {

    let urlWS: string = this.urlWebService_begin + AppUrlConfig.URL_API_OPERATION;
    const headers = { 'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'}
    return this._http.get<IOperation[]>(urlWS, {'headers':headers});

  }
  //Get Balance
  public getBalance(): Observable<number> {

    let urlWS: string = this.urlWebService_begin + AppUrlConfig.URL_API_BALANCE;
    const headers = { 'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'}
    return this._http.get<number>(urlWS, {'headers':headers});

  }

}
