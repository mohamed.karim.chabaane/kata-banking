import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AccountRequest} from "../models/accountRequest";
import {Observable} from "rxjs";
import {IAccount} from "../models/account";
import {AppUrlConfig} from "../app.url-config";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private urlWebService_client: string = AppUrlConfig.URL_WEBSERVICE_BEGIN + AppUrlConfig.URL_API_CLIENT;

  constructor(private _http: HttpClient) { }


  login(accountRequest: AccountRequest): Observable<IAccount> {
    let urlWS: string = this.urlWebService_client ;
    const headers = { 'content-type': 'application/json',
                      'Access-Control-Allow-Origin': '*',
                      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'}
    return this._http.post<IAccount>(urlWS, JSON.stringify(accountRequest), {'headers':headers});
  }
}
