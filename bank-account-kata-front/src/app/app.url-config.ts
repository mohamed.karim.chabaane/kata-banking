
export class AppUrlConfig {

   public static URL_WEBSERVICE_BEGIN='http://localhost:8080';
   public static URL_API_OPERATION='/Bank-Kata/operations';
   public static URL_API_DEPOSIT='/Bank-Kata/deposit';
   public static URL_API_WITHDRAW='/Bank-Kata/withdraw';
   public static URL_API_BALANCE='/Bank-Kata/balance';
   public static URL_API_CLIENT='/client/login'

}
